<?php

class Home extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('m_user');
    }

    public function index(){
        $this->load->view('beranda');
    }

    public function LoginKonsultan(){
        $this->load->view('LoginAdmin');
    }

    public function LoginPasien(){
        $this->load->view('LoginPasien');
    }

    public function Pasien(){
        $username = $this->session->userdata('user_name');
        $data['konsultan'] =$this->m_user->getKonsultan();
        $data['profile'] = $this->m_user->find($username);
        $this->load->view('Pasien',$data);
    }

    public function Admin(){
        $this->load->view('Admin');
    }
    public function RegisterPasien(){
        $this->load->view('RegisterPasien');
    }
    public function doLogin(){
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_message('required', 'Enter %s');
            if ($this->form_validation->run() == false) {
                redirect('Home/login');
            } else {
                $username    = $this->input->post('username');
                $password = $this->input->post('password');
                $user     = $this->db->get_where('user', ['username' => $username])->row_array();
                var_dump($username);
                var_dump($user);
                if ($user) {
                    if (password_verify($password, $user['password'])) {
                        $currentUser = $this->m_user->find($username);
                        $role = $currentUser->level;
                        $username = $currentUser->username;
                        $this->session->set_userdata('user_name', $username);
                        $this->session->set_userdata('user_level', $role);
                        switch($role) {
                            case '1' :
                            $this->session->set_userdata('user_level',1); 
                            redirect('Home/Pasien');
                            break;
                            case '2' :
                                $this->session->set_userdata('user_level',2); 
                            redirect('Home/Pasien1');
                        }
                       
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert">
                Wrong Password!
              </div>');
                        redirect('Home/Login2');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert">
                Email is not registered!
              </div>');                }
            }

    }
    public function doRegister(){
        $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('nohp', 'Nomor HP', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
        $this->form_validation->set_message('required', 'Enter %s');
        if ($this->form_validation->run() == false){
            redirect('Home/Admin');
        } else{
            $data = [
                'alamat' => $this->input->post('alamat'),
                'username' => $this->input->post('username'),
                'nama' => $this->input->post('fullname'),
                'email' => $this->input->post('email'),
                'hp' => $this->input->post('nohp'),
                'level' => '1',
                'password' =>password_hash($this->input->post('password'),PASSWORD_DEFAULT), 
            ];
            $this->db->insert('user', $data); 
        }
    }

    public function AboutUs(){
        $this->load->view('AboutUs');
    }
    public function getConsultant($id){
        $data['consultation'] = $this->m_user->findKonsultan($id);
        $this->session->set_userdata('konsultan_id',($data['consultation']->konsultan_id));
        $this->load->view('form',$data);
    }
    public function addKonsultasi(){
        $this->form_validation->set_rules('complaint', 'Complain', 'required');     
        if ($this->form_validation->run() == false){
            redirect('Home');
        }else {
            $data = array (
                'date' => date('Y-m-d H:i:s'),
                'complaint' =>$this->input->post('complaint'),
                'pasien_id' =>$this->session->userdata('user_name'),
                'konsultan_id' =>$this->session->userdata('konsultan_id')
            );
            $this->db->insert('history',$data); 
        }
    }
    public function editProfile(){
        $this->load->view('editProfile');
    }
    public function changeProfile(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
        $upload['upload_path'] = './assets/';
        $upload['allowed_types'] = 'jpg|png';
        $username = $this->session->userdata('user_name');
        $upload['file_name'] = $username;
        $this->load->library('upload', $upload);
        if ($this->upload->do_upload('uploadPhoto')) {
            $image = $this->upload->data();
            $data_user = [
                'nama' => $this->input->post('name'),
                'alamat' => $this->input->post('address'),
                'email' => $this->input->post('email'),
                'photo' => $image['file_name']
                ];
                $this->m_user->updateUser($username, $data_user);
                var_dump($data_user);
        
        }   
    }
}