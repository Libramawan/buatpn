<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css" />

    <title>Login Admin</title>
  </head>
  <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="Index">MENTAL HEALTH CONSULTANT</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarColor01">
                  <ul class="navbar-nav mr-auto">
                
                  </ul>
                  <ul class="form-inline navbar-nav">
                        <li class="nav-item">
                                <a class="nav-link" href="Index">Home</a>
                             </li>
                        <li class="nav-item">
                            <a class="nav-link" href="RegisterPasien">Register</a>
                         </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Login</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="LoginPasien">Pasien</a>
                                    <a class="dropdown-item" href="LoginAdmin">Admin</a>
                                </div>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" href="AboutUs">About Us</a>
                        </li>
                        
                    </ul>
        
                </div>
              </nav>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
                <div class="card border-primary mb-3" style="max-width: 20rem;">
                        <div class="card-header text-center text-primary"><h4>Login Admin</h4></div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label >Username</label>
                                    <input type="email" class="form-control"  placeholder="Masukan Username">
                                 </div>
                                <div class="form-group">
                                    <label >Password</label>
                                    <input type="password" class="form-control"  placeholder="Masukkan Password">
                                </div>
                                <br>
                                <a href="Admin" type="button" class="btn btn-primary btn-lg btn-block" >Login</a>
                        </div>  
                        </div>
                </div>
              
        <div class="col-4"></div>
    </div>

</div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <footer class=" card text-white bg-primary">
        
             <br>
                <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
            <br>
            </footer>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html>