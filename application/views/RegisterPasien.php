<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css" />

    <title>Register Pasien</title>
  </head>
  <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="Index">MENTAL HEALTH CONSULTANT</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarColor01">
                  <ul class="navbar-nav mr-auto">
                
                  </ul>
                  <ul class="form-inline navbar-nav">
                        <li class="nav-item">
                                <a class="nav-link" href="Index">Home</a>
                             </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Register</a>
                         </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Login</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="LoginPasien">Pasien</a>
                                    <a class="dropdown-item" href="LoginKonsultan">Konsultan</a>
                                </div>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" href="AboutUs">About Us</a>
                        </li>
                        
                    </ul>
        
                </div>
              </nav>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
                <div class="card border-primary" >
                        <div class="card-header text-center text-primary"><h4>Register Pasien</h4></div>
                            <div class="card-body">
                            <form class= "register" action="<?php echo base_url('Home/doRegister')?>" method="post">
                                <div class="form-group">
                                    <label >Username</label>
                                    <input type="text" name="username" class="form-control"  placeholder="Masukkan Username">
                                 </div>
                                 <div class="form-group">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name = "fullname" class="form-control"  placeholder="Masukkan Nama Lengkap">
                                     </div>
                               <div class="form-group">
                                    <label >Alamat</label>
                                    <input type="text" class="form-control"  name ="alamat" placeholder="Masukkan Alamat">
                                 </div>
                                 <div class="form-group">
                                        <label >Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Masukkan Email">
                                     </div>

                                  <div class="form-group">
                                        <label >Nomor Hp</label>
                                        <input type="email" class="form-control" name="nohp" placeholder="Masukkan Nomor HP">
                                     </div>
                                 
                                 
                                <div class="form-group">
                                    <label >Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Masukkan Password">
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                                </div>
                            </form>
                        </div>
                </div>
              
        <div class="col-3"></div>
    </div>

</div>

<div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <footer class=" card text-white bg-primary">
        
             <br>
                <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
            <br>
            </footer>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html>