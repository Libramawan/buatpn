<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fa/css/fontawesome.min.css">

    <title>MENTAL HEALTH CONSULTANT</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">MENTAL HEALTH CONSULTANT</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarColor01">
          <ul class="navbar-nav mr-auto">
        
          </ul>
          <ul class="form-inline navbar-nav">
                <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                     </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('home/RegisterPasien');?>">Register</a>
                 </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Login</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo base_url('home/LoginPasien');?>">Pasien</a>
                            <a class="dropdown-item" href="<?php echo base_url('home/LoginKonsultan');?>">Konsultan</a>
                        </div>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('home/AboutUs');?>">About Us</a>
                </li>
                
            </ul>
          <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
          </form> -->
        </div>
      </nav>
<br>
<div class="container ">
    <img style="height: 500px; width: 100%; display: block;" src="<?php echo base_url();?>assets/img/konsultasi.jpg">
</div>
<br>
<div class="container">
    <div class="row">

    <div class="col-4">
            <div class="card text-white bg-primary ">
                    <div class="card-body">
                      <h4 class="card-title">Kenali Ciri-Ciri Gangguan Mental</h4>
                      <p class="card-text">1. Merasa sedih dan tak punya harapan</p>
                      <p class="card-text">2. Muncul niat mengakhiri hidup</p>
                      <p class="card-text">3. Tak bisa mengendalikan diri</p>
                      <p class="card-text">4. Takut tanpa alasan</p>
                      <p class="card-text">5. Perubahan pola makan ekstrem</p>
                      <p class="card-text">6. Menyalahgunakan narkoba dan minum minuman beralkohol</p>
                      <p class="card-text">7. Perubahan perilaku yang esktrem</p>
                    </div>
                  </div>
    </div>
    <div class="col-4">
            <div class="card text-white bg-primary ">
                    <div class="card-body">
                      <h4 class="card-title">Konsultasi psikologi</h4>
                      <p class="card-text">Konsultasi psikologi merupakan layanan yang dapat digunakan oleh berbagai pihak, 
                      baik antar tenaga medis profesional dalam menangani kondisi pasien, 
                      maupun pihak-pihak lain yang membutuhkan. Kesehatan mental memiliki 
                      pengaruh besar terhadap kualitas hidup seseorang. Oleh karena itu, jangan segan melakukan konsultasi psikologi jika memang dibutuhkan.</p>
                    </div>
                  </div>
    </div>
    <div class="col-4">
            <div class="card text-white bg-primary ">
                    <div class="card-header">Header</div>
                    <div class="card-body">
                      <h4 class="card-title">Anda Depresi !!!
                      <br> HUBUNGI KITA </br>  
                      </h4>
                      <p class="card-text"></p>
                    </div>
                  </div>
    </div>

    </div>

</div>
<br>
<div>
<footer class=" card text-white bg-primary text">
    <br>
        <p class="text-center">Copyright @ 2019 - MENTAL HEALTH CONSULTANT</p>
    <br>
    </footer>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js" ></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" ></script>
  </body>
</html>