<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_konsultan extends CI_Model
{
    public function get()
    {
        $hasil = $this->db->get('konsultan');
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function find($konsultan_id)
    {
        $hasil = $this->db->where('konsultan_id', $konsultan_id)
            ->limit(1)
            ->get('konsultan');
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }
    public function update($konsultan_id, $data_konsultans)
    {
        $this->db->where('konsultan_id',$konsultan_id)
            ->update('konsultan', $data_konsultans);
        }
    public function delete($konsultan_id)
    {
        $this->db->where('konsultan_id', $konsultan_id)->delete('konsultan');
    }
}
