-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2019 at 07:00 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webkonsultan`
--

CREATE DATABASE webkonsultan;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `history_id` bigint(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `complaint` varchar(255) NOT NULL,
  `suggestion` varchar(5000) NOT NULL,
  `pasien_id` int(11) NOT NULL,
  `konsultan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`history_id`, `date`, `complaint`, `suggestion`, `pasien_id`, `konsultan_id`) VALUES
(1, '2019-12-01 18:25:57', '2323232', '', 0, 1),
(2, '2019-12-01 18:30:05', '444', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `konsultan`
--

CREATE TABLE `konsultan` (
  `konsultan_id` int(11) NOT NULL,
  `category` varchar(40) NOT NULL,
  `workhour` varchar(255) NOT NULL,
  `avail` tinyint(1) NOT NULL,
  `username` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsultan`
--

INSERT INTO `konsultan` (`konsultan_id`, `category`, `workhour`, `avail`, `username`) VALUES
(1, 'konsultan1', 'konsultan1', 1, 'konsultan1'),
(2, 'konsultan2', 'konsultan2', 0, 'konsultan2');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('1','2','3') NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `level`, `nama`, `alamat`, `email`, `hp`, `photo`) VALUES
('', '$2y$10$kYyW.kjkQYF4Nk7TOaiG/eVNr8r6lyb6uXT/XeqjYO4DkcxCiJP8O', '1', 'putrinabila668@gmail.com', 'putrinabila668@gmail.com', 'putrinabila668@gmail.com', 'putrinabila668@', ''),
('konsultan1', '12345678', '2', 'Bambang', 'konsultan1', 'konsultan1', 'konsultan1', ''),
('konsultan2', 'konsultan1', '2', 'konsultan23232', 'konsultan1', 'konsultan1', 'konsultan1', ''),
('putrinabila4', '$2y$10$RRS2FimUpzqJtvyss7LZIOd9EhkDiIFRPb1eVXGdlAdkwqN.OREQO', '1', 'Putri', '123213', '123123', '085260126004@gm', 'putrinabila44.png'),
('putrinabila668', '$2y$10$61TuqOyFi67Tcq6SxKMeV.52zDyS3dtkBCIp9QkShx3WxJJpokm9C', '1', 'putrinabila668', 'putrinabila668', 'putrinabila668@gmail.com', 'putrinabila668@', ''),
('putrinabila6682@gmai', '$2y$10$dk8jYB2qUIeWx1Oj.Z.TE.uR3zeKiqmDZTf35hSlubjcCu7pO8o8S', '1', 'putrinabila6682@gmail.com', 'putrinabila6682@gmail.com', 'putrinabila6682@gmail.com', 'putrinabila6682', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `konsultan`
--
ALTER TABLE `konsultan`
  ADD PRIMARY KEY (`konsultan_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `history_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `konsultan`
--
ALTER TABLE `konsultan`
  MODIFY `konsultan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
